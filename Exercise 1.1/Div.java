package com.deloitte.week2;
public class Div extends Arithmetic {
	@Override
	public int calc(int x, int y) throws ArithmeticException{
		
		try{
			return (int)x/y;
		}
		catch(ArithmeticException e) {
			System.out.println(" Denominator shouldn't be zero");
			return 0;
		}
	}

}
