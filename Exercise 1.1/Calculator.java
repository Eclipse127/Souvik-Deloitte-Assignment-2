package com.deloitte.week2;
import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
		
		Object[] fs = {new Add(), new Sub(), new Mult(), new Div()};
		System.out.println("Enter your choice:\n1. Add\n2. Sub\n3. Multiply\n4. Divide");
		int input;
		Scanner sc = new Scanner(System.in);
		input = sc.nextInt();
		int num1, num2;
		System.out.println("Enter two numbers: ");
		num1 = sc.nextInt();
		num2 = sc.nextInt();
		int y = ((Arithmetic)fs[input-1]).calc(num1, num2);
		System.out.println(y);
		sc.close();

	}
}
