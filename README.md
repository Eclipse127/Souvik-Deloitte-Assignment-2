# Souvik-Deloitte-Assignment-2


 
# Exercise 1.1:

***Problem Statement:
Develop a calculator  application to perform addition, subtraction, multiply and divide two number based on the choice 1 add 2 sub 3 mul 4 div   without using any control statement like if, else if, case***

*1)Create an abstract class Arithmetic with has 2 instance variable num1, num2, num3. Methods to read, and display with the constructor to initialize. Abstract method calculate*
*2)	Create sub class for addition, subtraction, multiplication and divide and override calculate method.*
*3)	Create a array of reference for arithmetic class and initialize object of sub call (addition, subtraction, multiplication and division)*
*4)	Read two numbers and choice perform the calculation base on choice without using control statement.*
# Exercise 1.2:  

***Problem Statement:Develop java application to compute income tax of employee based on annual income.Create class EmplyeeVo which contain instance variable empid, empname, annualincome, incometax. Create setter and getter methods***

*Override the following methods:*
*1)	 toString method which returns all the employee details.*
*2)	Hashcode method to return the hashcode of the data*
*3)	Equals method to compare the object*
*4)	Add the parameter constructor to add data*

 ***Create class EmplyeeBo which contains instance method calincomeTax (), return type is void, argument is EmplyeeVo. Take the annualincome for emplyeeVo and compute income tax and store it back to emplyeeVo.***

***Create class Emplyeesort implement comparator interface and override compareTo method do sort the data on incometax in descending order.
Create an EmplyeeMain with main method and do the following***
*1)	Accept no of employees*
*2)	Create an array of EmplyeeVo based on number of employees*
*3)	Read empid, empname, annualincome and calculate income tax calling calincomeTax method of EmplyeeBo object.*
*4)	Display the data of the all the employee*
*5)	Sort the data of the employee based on income tax and re print.*


# Exercise 1.3:  
 
***Given an integer array, Write a program to find if the array has any triplets. A triplet is a value if it appears 3 consecutive times in the array.***

***Include a class UserMainCode with a static method checkTripplets which accepts an integer array. The return type is boolean stating whether its a triplet or not.***

***Create a Class Main which would be used to accept the input arrayand call the static method present in UserMainCode.***

***Input and Output Format:
Input consists of n+1 integers. The first integer would represent the size of array and the next n integers would have the values.
Output consists of a string stating TRUE or FALSE.
Refer sample output for formatting specifications.***

*Sample Input 1:*
7
3
3
5
5
5
2
3

*Sample Output 1:*
TRUE

*Sample Input 2:*
7
5
3
5
1
5
2
3

*Sample Output 2:*
FALSE
